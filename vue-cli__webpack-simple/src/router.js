import Vue from 'vue'
import Router from 'vue-router'
import StepOne from './components/StepOne.vue'
import StepConfirm from './components/StepConfirm.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: StepOne
    },
    {
      path: '/confirm',
      component: StepConfirm
    }
  ]
})
