import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex)

let initialState = {
  form: {
    name: '',
    firstName: ''
  },
  isConfirmed: false
}

export default new Vuex.Store({
  state: initialState,
  mutations: {
    updateField (state, payload) {
      state.form[payload.key] = payload.value
    },
    submitForm (state, payload) {
      state.form = payload
    },
    cancel (state) {
      state.form = {
        name: '',
        firstName: ''
      }

      state.isConfirmed = false

      // Come back on the first step
      router.push({path: '/'})
    },
    confirm (state) {
      state.isConfirmed = true

      // Emit a notification or something here
      console.log('Form submitted!')

      // Come back on the first step
      router.push({path: '/'})
    }
  }
})
